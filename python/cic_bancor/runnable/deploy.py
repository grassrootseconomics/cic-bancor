# standard imports
import os
import logging
import json
import argparse

# third-party imports
import web3
import confini
from cic_registry.bancor import BancorRegistry, BancorDeployer
from cic_registry import bancor

script_dir = os.path.dirname(os.path.realpath(__file__))
root_dir = os.path.dirname(script_dir)
bancor_default_dir = os.path.join(root_dir, '..', 'bancor')

logging.basicConfig(level=logging.WARNING)
logg = logging.getLogger()

logging.getLogger('web3').setLevel(logging.WARNING)
logging.getLogger('urllib3').setLevel(logging.WARNING)

argparser = argparse.ArgumentParser()
argparser.add_argument('-p', '--provider', dest='p', default='http://localhost:8545', type=str, help='Web3 provider url (http only)')
argparser.add_argument('-r', '--reserve-address', dest='r', required=True, type=str, help='reserve token address')
# TODO: make url (git?) fetch possible
argparser.add_argument('--bancor-dir', dest='bancor_dir', type=str, default=bancor_default_dir, help='absolute path to root of bancorprotocol contracts-solidity')
argparser.add_argument('-v', action='store_true', help='Be verbose')
args = argparser.parse_args()

if args.v:
    logg.setLevel(logging.DEBUG)

def main():
    provider = web3.Web3.HTTPProvider(args.p)
    w3 = web3.Web3(provider)
    w3.eth.defaultAccount = w3.eth.accounts[0]

    deployer = BancorDeployer(w3, args.bancor_dir)
    r = deployer.deploy()

    deployer.set_default_reserve(args.r)

    #ta = deployer.create_token(args.r, w3.eth.accounts[0], 'Foo Token', 'FOO', 18, 1000000)
    #tb = deployer.create_token(args.r, w3.eth.accounts[0], 'Bar Token', 'BAR', 18, 1000000)

    print(r)

#    print(json.dumps({
#        'registry': r,
#        'tokens': [ta, tb],
#        }))
if __name__ == '__main__':
    main() 
