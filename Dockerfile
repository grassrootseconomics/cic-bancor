FROM python:3.8.6-slim-buster

RUN apt-get update && apt-get -y install git gcc libpq-dev wget python2 make g++ gnupg

RUN wget https://nodejs.org/dist/v10.16.1/node-v10.16.1-linux-x64.tar.gz && \
	tar -zxvf node-v10.16.1-linux-x64.tar.gz && \
	cp -vR node-v10.16.1-linux-x64/bin/* /usr/bin/ && \
	cp -vR node-v10.16.1-linux-x64/lib/* /usr/lib/ 

WORKDIR /usr/src

COPY ["./bancor/package.json", "./bancor/package-lock.json*", "./bancor/"]

RUN cd bancor && \
	npm install --python=/usr/bin/python2

COPY bancor/ bancor/

RUN cd bancor && \
	node_modules/truffle/build/cli.bundled.js compile

# # These files can be copied by other images needing the contract metadata (those using cic-registry)
RUN mkdir -vp /usr/local/share/cic/bancor/solidity/build && \ 
	cp -vR bancor/solidity/build/contracts /usr/local/share/cic/bancor/solidity/build/

FROM python:3.8.6-alpine

COPY --from=0 /usr/local/share/cic/bancor/ /usr/local/share/cic/bancor/

RUN apk update && \
	apk add gcc musl-dev gnupg libpq bash

